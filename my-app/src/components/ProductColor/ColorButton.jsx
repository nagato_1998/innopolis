import "./ColorButton.css";

function ColorButton(props) {
  const { item, isActive } = props;

  return (
    <div className={`product__image ${isActive ? "selected" : ""}`}>
      <img
        className="img-button"
        src={item.src}
        alt="Color of product"
        key={item.src}
      />
    </div>
  );
}

export default ColorButton;

