import { useState } from "react";
import "./ProductColor.css";
import ColorButton from "./ColorButton";

function ProductColor(props) {

  const { buttons } = props;

  const [idActive, setIdActive] = useState(JSON.parse(localStorage.getItem("colorChoise")));

  const [colorText, setColorText] = useState("");
  
  localStorage.setItem("colorChoise", JSON.stringify(idActive));

  const colorArr = [
    "красный",
    "зеленый",
    "розовый",
    "синий",
    "белый",
    "черный",
  ];

  const handleClick = (id) => {
    setColorText(colorArr[id - 1]);
    setIdActive(id);
  };


  return (
    <div>
      <section className="product-color">
        <div className="product__wrapper">
          <div className="product__subtitle-1">Цвет товара: {colorText}</div>
        </div>

        <div className="product__pictures">
          {buttons.map((item) => {
            return (
              <div onClick={() => handleClick(item.id)} key={item.id}>
                <ColorButton item={item} isActive={item.id === idActive} />
              </div>
            );
          })}
        </div>
      </section>
    </div>
  );
}
export default ProductColor;
