import './breadCrumbs.css';
import Link from '../Link/Link';


function BreadCrumbs(props) {
    let { list } = props;

    let list2 = [];
    list.map((value, index) => {

        return (
            list2.push(<Link text={value.text} link={value.link} key={value.text} />) &&
            list2.push(<span key={index}>{'>'}</span>)
        )
    });

    list2.pop();

    return (
        <nav className="breadcrumbs">
            {list2}
        </nav>
    );

}

export default BreadCrumbs;