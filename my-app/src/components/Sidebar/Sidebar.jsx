import { useSelector, useDispatch } from "react-redux";
import { addProducts, addLikes } from "../../cart-reducer.js";
import "./Sidebar.css";
import Iframe from "../Iframe/Iframe";
import MessageCart from "../MessageCart/MessageCart";
import { useState } from "react";

function Sidebar() {

  const cart = useSelector((store) => store.cart);
  const dispatch = useDispatch();
  const [clicks, setClicks] = useState(0)



  const handleClickProducts = () => {
    dispatch(addProducts());
    setClicks((prevState) => prevState + 1);
  };
  const handleClickLikes = () => dispatch(addLikes());


  return (
    <div className="product__sidebar">
      <div className="product__price price">
        <div className="price__group">
          <div className="price__discount">
            <s className="gray-1">
              <b>75 990₽</b>
            </s>
            <div className="price__percent-wrapped">
              <p className="price__percent">-8%</p>
            </div>
            <div
              onClick={handleClickLikes}
              className={`sidebar-info__like ${
                cart.likes===1 ? "like_choise" : ""
              }`}
            ></div>
          </div>
          <div className="price__sum">67 990₽</div>
        </div>
        <div className="price__comment">
          <p>
            Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
          </p>
          <p>
            Курьером в четверг, 1 сентября — <b>бесплатно</b>
          </p>
        </div>
        <button
          onClick={handleClickProducts}
          className={` ${cart.products===1  ? 'choise' : 'sidebar__button'}`} 
        >
         {` ${cart.products===1  ? 'Убрать из корзины' : 'Добавить в корзину'}`}  
        </button>
      </div>

      <div className="advertisement">
        <p>Реклама</p>
        <div className="advertisement__iframes">
          <div className="ads">
            <Iframe />
          </div>
          <div className="ads">
            <Iframe />
          </div>
        </div>
      </div>
      <MessageCart clicks={clicks} />
    </div>
  );
}

export default Sidebar;