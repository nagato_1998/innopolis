import "./ProductCharacteristics.css";

function ProductCharacteristics() {
  return (
    <div>
      <div className="product__property characteristic">
        <div className="product__subtitle">Характеристики товара</div>
        <ul className="product__characteristics">
          <li>
            <span>
              Экран: <b>6.1</b>
            </span>
          </li>
          <li>
            <span>
              Встроенная память: <b>128 ГБ</b>
            </span>
          </li>
          <li>
            <span>
              Операционная система: <b>iOS 15</b>
            </span>
          </li>
          <li>
            <span>
              Беспроводные интерфейсы:{" "}
              <b>
                <span className="unfo">NFC,</span>Bluetooth, <span className="unfo-1">Wi-Fi</span>
              </b>
            </span>
          </li>
          <li>
            <span>
              Процессор:{" "}
              <b>
                <a href="https://ru.wikipedia.org/wiki/Apple_A15" id="wiki">
                  A15 Bionic
                </a>
              </b>
            </span>
          </li>
          <li>
            <span>
              Вес: <b>173 г</b>
            </span>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default ProductCharacteristics;
