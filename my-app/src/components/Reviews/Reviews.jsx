import "./Reviews.css";
import productData from "../Data";
// import Review from './Review';

function Reviews() {
  return productData.reviews.map((review, index) => {
    return (
      <div className="review__description" key={review.id}>
        <div className="reviews__item review" key={review.name}>
          <img
            src={review.photo}
            alt="Фотография"
            className="review__author-photo"
          />
          <div className="review__comment">
            <div className="review__author">
              <div className="review__author-name">{review.name}</div>
              <img
                src={review.rating}
                alt="Рейтинг (звезды)"
                className="review__rating-img"
              />
            </div>

            <div className="review__details">
              <p>
                <b>Опыт использования: </b>
                {review.periodUse}
              </p>
              <p>
                <b>Достоинства:</b>
                <br /> {review.plus}
              </p>
              <p>
                <b>Недостатки:</b>
                <br /> {review.minus}
              </p>
            </div>
          </div>
        </div>
        {index !== productData.reviews.length - 1 && (
          <div className="reviews__separator" key={review.id}>
            <div className="review__line"></div>
          </div>
        )}
      </div>
    );
  });
}

export default Reviews;
