import styled from "styled-components";

let Table = styled.table`
  width: 876px;
  height: 190px;
  border: 1px solid #888888;
  margin-top: 20px;
  margin-bottom: 60px;
  border-collapse: collapse;
`;

let Th = styled.th`
  text-align: center;
  border: 1px solid #888888;
  padding: 10px;
  white-space: nowrap;
`;

let Td = styled.td`
  text-align: center;
  border-right: 1px solid #888888;
  padding: 10px;
  white-space: nowrap;
`;

let Line = styled.tr`
  &:hover {
    background: #f2f2f2;
  }
`;

function ProductComparison() {
  return (
    <Table>
      <thead>
        <tr>
          <Th>Модель</Th>
          <Th>Вес</Th>
          <Th>Высота</Th>
          <Th>Ширина</Th>
          <Th>Толщина</Th>
          <Th>Чип</Th>
          <Th>Объём памяти</Th>
          <Th>Аккумулятор</Th>
        </tr>
      </thead>

      <tbody>
        <Line>
          <Td>Iphone11</Td>
          <Td>194 грамма</Td>
          <Td>150.9 мм</Td>
          <Td>75.7 мм</Td>
          <Td>8.3 мм</Td>
          <Td>A13 Bionicchip</Td>
          <Td>до 128 Гб</Td>
          <Td>До 17 часов</Td>
        </Line>
        <Line>
          <Td>Iphone12</Td>
          <Td>164 грамма</Td>
          <Td>146.7 мм</Td>
          <Td>71.5 мм</Td>
          <Td>7.4 мм</Td>
          <Td>A14 Bionicchip</Td>
          <Td>до 256 Гб</Td>
          <Td>До 19 часов</Td>
        </Line>
        <Line>
          <Td>Iphone13</Td>
          <Td>174 грамма</Td>
          <Td>146.7 мм</Td>
          <Td>71.5 мм</Td>
          <Td>7.65 мм</Td>
          <Td>A15 Bionicchip</Td>
          <Td>до 512 Гб</Td>
          <Td>До 19 часов</Td>
        </Line>
      </tbody>
    </Table>
  );
}

export default ProductComparison;
