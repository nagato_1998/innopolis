import './footer.css';
import Link from '../Link/Link'
import { useCurrentDate } from '@kundinos/react-hooks';

function Footer(props) {const currentDate = useCurrentDate();
    const fullYear = currentDate.getFullYear();
    const fullMonth = currentDate.getMonth();
    const fulldate = currentDate.getDate();
    const hours = currentDate.getHours();
  const minutes = currentDate.getMinutes();
  const seconds = currentDate.getSeconds();
    let { list } = props;
    return (
        <footer className="footer" id="ten">
        <div className="footer__container container">
            <div className="footer__contact-info">
                <p><b>© ООО «<span className="orange-1">Мой</span>Маркет», 2018-<strong className="footer_Dat">(Месяц-{fullMonth}.Год-{fullYear}.День-{fulldate}) ; Текущее время: {hours}:{minutes}:{seconds}.</strong></b><br />
                    Для уточнения информации звоните по номеру <Link text={list.tel.text} link={list.tel.link}/>,</p>
                <p>а предложения по сотрудничеству отправляйте на почту <Link text={list.mail.text} link={list.mail.link} /></p>
            </div>
            <a href="#top" className="footer__list">Наверх</a>
        </div>
        </footer>
    );
  }
  
  export default Footer;
