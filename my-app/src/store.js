import { configureStore } from "@reduxjs/toolkit";
import cartReducer from './cart-reducer';
import storage from 'redux-persist/lib/storage';

import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';

let count = [];

const logger = (store) => (next) => (action) => {
    count.push(action);
    console.log("action", action);
    let result = next(action);
    console.log("next state", store.getState());
    console.log("Количество обработанных действий:", count.length);
    return result;
};

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, cartReducer)

export const store = configureStore({
    reducer: {
        cart: persistedReducer
    },

    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }),
    middleware: [logger],
});


export const persistor = persistStore(store);

export default store;