"use strict";

//Упражнение01
let a1 = "$100";
let b2 = "300$";
//Решение
let summ = Number(a1.slice(1, 4)) + Number(b2.slice(0, 3));

console.log(summ);

//Упражнение02
let message = " привет, медвед        ";
// Решение
message = message.trim();
message = message[0].toUpperCase() + message.slice(1);

console.log(message);// “Привет, медвед”

//Упражнение03
let years = prompt("Сколько вам лет?", 0);
// Решение 
// Преобразуем полученную от пользователя строку в число
let years_num = Number(years);

// Проверяем корректность ввода
if (isNaN(years_num)) {
  console.log("Вы ввели возраст неправильно");
} else {
  // Если пользователь ввёл возраст корректно, значит прописываем условия для ответа
  if (years_num >= 0 && years_num <= 3) {
    alert("Вам " + years_num + " лет и вы младенец");
  } else if (years_num >= 4 && years_num <= 11) {
    alert("Вам " + years_num + " лет и вы ребёнок");
  } else if (years_num >= 12 && years_num <= 18) {
    alert("Вам " + years_num + " лет и вы подросток");
  } else if (years_num >= 19 && years_num <= 40) {
    alert("Вам " + years_num + " лет и вы познаёте жизнь");
  } else if (years_num >= 41 && years_num <= 80) {
    alert("Вам " + years_num + " лет и вы познали жизнь");
  } else if (years_num >= 81) {
    alert("Вам " + years_num + " лет и вы долгожитель");
  }
}

//Упражнение04
let message1='Я работаю со строками как профессионал!';
// Решение
// Обрезаем у строки пробелы и возвращается массив из элементов перед и после пробелов
// Далее работая как с массивом, считаем количество слов - элементов через length
let count = message1.split(" ").length;
console.log(count);
