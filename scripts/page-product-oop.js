"use strict"

class FormBase {
    finish() {
        alert('Форма отправлена');
    }
}

class Form extends FormBase {

    constructor(
        formSelector,
        nameSelector,
        gradeSelector,
        nameMistakeSelector,
        gradeMistakeSelector
    ) { 
        super();
        this.form = document.querySelector(formSelector);
        this.name = document.querySelector(nameSelector);
        this.nameMistake = document.querySelector(nameMistakeSelector);
        this.grade = document.querySelector(gradeSelector);
        this.gradeMistake = document.querySelector(gradeMistakeSelector);
        this.name.value = localStorage.getItem('name');
        this.grade.value = localStorage.getItem('grade');
        this.form.addEventListener('submit', this.validate.bind(this));
        this.name.addEventListener('focus', this.clearMistakes.bind(this));
        this.grade.addEventListener('focus', this.clearMistakes.bind(this));
        this.name.addEventListener('input', this.changeName.bind(this));
        this.grade.addEventListener('input', this.changeGrade.bind(this));
    }
 
    validate(event) {
        event.preventDefault()
    
        let nameLength = this.name.value.trim().length;
        let NameSymbols = this.name.value.trim();
        let grade = + +this.grade.value.trim();
    
        if (nameLength >= 3) {
            console.log('OK');
    
        } else if (NameSymbols === '') {
            this.enoughMistake01Name();

            return;
    
        } else {
            this.enoughMistake02Name();

            return;
        }
    
        for (let i = 0; i < nameLength; i++) {
    
            let codeOfSymbol = NameSymbols.charCodeAt(i);
    
            if (codeOfSymbol === 32) {
    
            } else if (codeOfSymbol > 64 && codeOfSymbol < 90) {
    
            } else if (codeOfSymbol > 96 && codeOfSymbol < 122) {
    
            } else if (codeOfSymbol > 1039 && codeOfSymbol < 1103) {
    
            } else {
                this.enoughMistake03Name()

                return;
            }
        }
    
        if (grade > 0 && grade < 6) {
            console.log('OK');
    
        } else {
            this.enoughMistakeGrade();

            return;
        }
        // валидация пройдена
        this.form.reset();
        localStorage.removeItem('name');
        localStorage.removeItem('grade');
        console.log('OK');

        this.finish();
    };

    enoughMistake01Name() {

        this.nameMistake.classList.add('input-mistake-name-color');
        this.nameMistake.innerHTML = 'Вы забыли указать имя и фамилию';
        this.name.classList.add('form__name-mistake');
    };

    enoughMistake02Name() {
        this.nameMistake.classList.add('input-mistake-name-color');
        this.nameMistake.innerHTML = 'Имя не может быть короче двух символов';
        this.name.classList.add('form__name-mistake');
    };

    enoughMistake03Name() {
        this.nameMistake.classList.add('input-mistake-name-color');
        this.nameMistake.innerHTML = 'Используйте буквы русского или английского алфавита';
        this.name.classList.add('form__name-mistake');
    };

    enoughMistakeGrade() {
        this.gradeMistake.classList.add('input-mistake-grade-color');
        this.gradeMistake.innerHTML = 'Оценка должна быть от 1 до 5';
        this.grade.classList.add('form__grade-mistake'); 
    };

    clearMistakes(){
        this.nameMistake.classList.remove('input-mistake-name-color');
        this.name.classList.remove('form__name-mistake');
        this.nameMistake.innerHTML = '';
        this.gradeMistake.classList.remove('input-mistake-grade-color');
        this.grade.classList.remove('form__grade-mistake');
        this. gradeMistake.innerHTML = '';

    };

    changeName() {
        localStorage.setItem('name', this.name.value);// сохранить ключ и значение.
    };

    changeGrade() {
        localStorage.setItem('grade', this.grade.value);
    };
};

    let review = new Form(
        '.form',
        '.form__name',
        '.form__grade',
        '.input-mistake-name',
        '.input-mistake-grade',
    );