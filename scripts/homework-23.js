"use strict";

// Упражнение01
// Просим пользователя ввести число для таймера
let number = prompt('Введите число');
let intervalId = setInterval(function () {
  // Проверка на правильно введёное число (как тип данных)
  if (isNaN(number)) {
    alert("Вы ввели не число :(");
    clearInterval(intervalId);
  };

  console.log('Осталось', number);
  number--;
  // Проверяем, если number дошло до нуля
  if (number === 0) {
    console.log('Время вышло!');
    // Остановка таймера
    clearInterval(intervalId);
  }
  // второй аргумент для setInterval это количество миллисекунд через которое он снова запускается
}, 1000);

//Упражнение02
let time = performance.now();

let promise = fetch("https://reqres.in/api/users");
// promise = fetch("https://rees.in/api/users"); // несуществующий адрес

promise

    .then(function (response) {
        return response.json();
    })


  .then(function (response) {
    console.log('Количество пользователей: ' + response.per_page);
    console.log(response.data); // для проверки
    response.data.forEach(function (element) {
      console.log(element.first_name + ' ' + element.last_name + ' ' + '(' + element.email + ')');
    });
  })

  .catch(function () {
    console.log("Кажется бэкенд сломался :(");
  });


time = performance.now() - time;
console.log('Время выполнения 2 упражнения равно ', time);
