"use strict"


let form = document.querySelector('form');// находим форму
let button = document.querySelector('button.reviews-form__submit1');

let inputName = document.querySelector('.form__name');
let mistakeName = document.querySelector('.input-mistake-name');

let inputGrade = document.querySelector('.form__grade');
let mistakeGrade = document.querySelector('.input-mistake-grade');

form.addEventListener('submit', validate);
inputName.addEventListener('focus', clearmistakeName);
inputGrade.addEventListener('focus', clearmistakeGrade);
inputName.addEventListener('input', changeName);
inputGrade.addEventListener('input', changeGrade);
inputName.value = localStorage.getItem('name');
inputGrade.value = localStorage.getItem('grade');

function validate(event) {

    event.preventDefault()

    let nameLength = inputName.value.trim().length;
    let NameSymbols = inputName.value.trim();
    let grade = +inputGrade.value.trim();

    if (nameLength >= 3) {
        console.log('OK');

    } else if (NameSymbols === '') {

        mistakeName.classList.add('input-mistake-name-color');
        mistakeName.innerHTML = 'Вы забыли указать имя и фамилию';
        inputName.classList.add('form__name-mistake');
        return;

    } else {
        mistakeName.classList.add('input-mistake-name-color');
        mistakeName.innerHTML = 'Имя не может быть короче двух символов';
        inputName.classList.add('form__name-mistake');
        return;
    }

    for (let i = 0; i < nameLength; i++) {

        let codeOfSymbol = NameSymbols.charCodeAt(i);

        if (codeOfSymbol === 32) {

        } else if (codeOfSymbol > 64 && codeOfSymbol < 90) {

        } else if (codeOfSymbol > 96 && codeOfSymbol < 122) {

        } else if (codeOfSymbol > 1039 && codeOfSymbol < 1103) {

        } else {
            mistakeName.classList.add('input-mistake-name-color');
            mistakeName.innerHTML = 'Используйте буквы русского или английского алфавита';
            inputName.classList.add('form__name-mistake');
            return;
        }
    }

    if (grade > 0 && grade < 6) {
        console.log('OK');

    } else {
        mistakeGrade.classList.add('input-mistake-grade-color');
        mistakeGrade.innerHTML = 'Оценка должна быть от 1 до 5';
        inputGrade.classList.add('form__grade-mistake');
        return;
    }
    // валидация пройдена
    form.reset();
    localStorage.removeItem('name'); // удалить данные с ключом
    localStorage.removeItem('grade');
    console.log('OK');
}

function clearmistakeName() {
    mistakeName.classList.remove('input-mistake-name-color');
    inputName.classList.remove('form__name-mistake');
    mistakeName.innerHTML = '';
}

function clearmistakeName() {
    mistakeGrade.classList.remove('input-mistake-grade-color');
    inputGrade.classList.remove('form__grade-mistake');
    mistakeGrade.innerHTML = '';
}

function clearmistakeGrade() {
    mistakeGrade.classList.remove('input-mistake-grade-color');
    inputGrade.classList.remove('form__grade-mistake');
    mistakeGrade.innerHTML = '';
}

function changeName() {
    localStorage.setItem('name', inputName.value); // сохранить ключ и значение
}

function changeGrade() {
    localStorage.setItem('grade', inputGrade.value);
}

// modul2

let sidebar = document.querySelector('.sidebar');
let price = document.querySelector('.price__heart');
let chooseButton = document.querySelector('.product__price > .btn_main');
let header = document.querySelector('.header');
let priceNumber = document.querySelector('.icon-2_hidden');
let IconNumber = document.querySelector('.icon-1_hidden');
let report = document.querySelector('.report_hidden');
price.value = +localStorage.getItem('priceCount');
chooseButton.value = +localStorage.getItem('productCount');
price.addEventListener('click', changePrice);
chooseButton.addEventListener('click', changeIcon);
console.log(price.value);
console.log(+chooseButton.value);

function changePrice(event) {

    if (price.value === 0) {
        price.value += 1;
        console.log(price.value);
        price.classList.add('price__heart_focus');
        priceNumber.classList.add('icon-2');
        localStorage.setItem('priceCount', (price.value));
    } else {
        price.value -= 1;
        console.log(price.value);
        price.classList.remove('price__heart_focus');
        priceNumber.classList.remove('icon-2');
        localStorage.removeItem('priceCount', (price.value));
        return;
    }
}

function changeIcon(event) {
    if (+chooseButton.value === 0) {
        chooseButton.value += 1;
        console.log(+chooseButton.value);
        report.classList.add('report');

        setTimeout(function () {
            report.classList.remove('report');
        }, 3300);

        chooseButton.classList.add('button_choose');
        IconNumber.classList.add('icon-1');
        chooseButton.innerHTML = 'Товар уже в корзине';
        localStorage.setItem('productCount', (+chooseButton.value));
    } else {
        chooseButton.value -= 1;
        console.log(+chooseButton.value);
        chooseButton.classList.remove('button_choose');
        IconNumber.classList.remove('icon-1');
        report.classList.remove('report');
        chooseButton.innerHTML = 'Добавить в корзину';
        localStorage.removeItem('productCount', (+chooseButton.value));
        return;
    }
}

document.addEventListener('DOMContentLoaded', () => {
    if (price.value === 0) {
        price.classList.remove('price__heart_focus');
        priceNumber.classList.remove('icon-2');
    } else {
        price.classList.add('price__heart_focus');
        priceNumber.classList.add('icon-2');
    }
    if (+chooseButton.value === 0) {
        chooseButton.classList.remove('button_choose');
        IconNumber.classList.remove('icon-1');
        chooseButton.innerHTML = 'Добавить в корзину';
    } else {
        chooseButton.classList.add('button_choose');
        IconNumber.classList.add('icon-1');
        chooseButton.innerHTML = 'Товар уже в корзине';
        return;
    }
});