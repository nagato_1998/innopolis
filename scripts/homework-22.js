"use strict";

//Упражнение01

/**
 * принимает любой массив и возвращает сумму чисел в этом массиве
 *  @return {Number} sum сумма чисел в этом массиве
 *  @param {Array} arr массив для расчета суммы
 */

//Решение
function getSumm(arr) {
    let sum = 0;
    arr.forEach(function (n) {
        if (typeof n === "number") {
            sum += n;
        }
    })
    return sum;
}

let arr1 = [1, 2, 10, 5];
alert(getSumm(arr1));// 18
let arr2 = ["a", {}, 3, 3, -2];
alert(getSumm(arr2));// 4

console.log(getSumm(arr1))
console.log(getSumm(arr2))

//Упражнение03
let cart= [4884];

//Решение
function addToCart(productId) {
    let hasInCart = cart.includes(productId);

    if (hasInCart) return;
    
    cart.push(productId);
}

function removeFromCart(productId){
  cart =  cart.filter(function(id) {
    return id !== productId;
  });

}
// Добавили товар 
addToCart(3456);
addToCart(3456);

console.log('1', cart);

removeFromCart(4884);

console.log('2', cart);
