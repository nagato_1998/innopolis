"use strict";

//Упражнение01
let i = 0
//Решение
for (let i = 0; i <= 20; i += 2) {
    if (i % 2 == 0)
    console.log(i);
}

//Упражнение02
let sum = 0;
//Решение
for (let i = 0; i < 3; i++) {

    let humber = +prompt("Введите число")

    if (humber ===""|| Number.isNaN(humber)) {
        alert('Ошибка. Вы ввели не число'); break;
    
    }
    sum =+ humber;

    if (sum) { console.log("Сумма: " + sum); break; }
    
}

//Упражнение03
let months = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
];
//Решение
function getNameOfMonth(month) {
    return console.log(months[month]);
}

let value = prompt("Введите порядковый номер месяца в году (от 0 до 11)", "");

getNameOfMonth(value);

for (let i = 0; i < months.length; i++) {
    if (i === 9) continue;
    console.log(months[i]);
}

//Упражнение04
(function () { /* code */ })();
// “Что такоеIIFE?”
// IIFE (Immediately Invoked Function Expression)-это JavaScript функция, которая выполняется сразу же после того, как она была определена.
